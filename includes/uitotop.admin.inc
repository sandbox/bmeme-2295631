<?php

/**
 * @file
 * UItoTop integration module.
 */

/**
 * UItoTop Settings form.
 */
function uitotop_settings_form($form, &$form_state) {

  $form['uitotop_text'] = array(
    '#title' => t('Text'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uitotop_text', 'To Top'),
  );

  $form['uitotop_min'] = array(
    '#title' => t('min'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uitotop_min', '200'),
    '#required' => TRUE
  );

  $form['uitotop_inDelay'] = array(
    '#title' => t('inDelay'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uitotop_inDelay', '600'),
    '#required' => TRUE
  );

  $form['uitotop_outDelay'] = array(
    '#title' => t('outDelay'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uitotop_outDelay', '400'),
    '#required' => TRUE
  );

  $form['uitotop_scrollSpeed'] = array(
    '#title' => t('scrollSpeed'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uitotop_scrollSpeed', '1200'),
    '#required' => TRUE
  );

  $form['uitotop_easingType'] = array(
    '#title' => t('easingType'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uitotop_easingType', 'linear'),
    '#required' => TRUE
  );

  return system_settings_form($form);
}
